<?php

use Illuminate\Database\Seeder;

class BBBSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('settings')->insert([
			[
		        'key' => 'is_bbb_configured',
		        'value' => YES
		    ],
		    [
		        'key' => 'BBB_SERVER_BASE_URL_PRODUCTION',
		        'value' => 'https://media.appswamy.com/bigbluebutton/'
		    ],
		    [
		        'key' => 'BBB_SECRET_PRODUCTION',
		        'value' => 'oJ7zwtTyDzaU4c6vQuyuiVnfVXziu33POa6tgh4o4'
		    ],
		    [
		        'key' => 'BBB_SERVER_BASE_URL_DEBUG',
		        'value' => 'https://media.appswamy.com/bigbluebutton/'
		    ],
		    [
		        'key' => 'BBB_SECRET_DEBUG',
		        'value' => 'Rm56l9lbFT6d2Cgi0haSTaAJSSw2zoeKB0Csbh3k'
		    ],
		    [
		        'key' => 'BBB_ENV',
		        'value' => 'production'
		    ],
		    [
		        'key' => 'meeting_recording',
		        'value' => 0
		    ],
		    [
		        'key' => 'BBB_RECORD_PRESENTATION_URL_PRO',
		        'value' => 'https://media.step.live/download/presentation/'
		    ],
		    [
		        'key' => 'BBB_RECORD_PRESENTATION_URL_DEBUG',
		        'value' => 'https://media.appswamy.com/download/presentation/'
		    ],
		    [
		        'key' => 'BBB_RECORD_VIDEO_URL_PRO',
		        'value' => 'https://media.appswamy.com/presentation/'
		    ],
		    [
		        'key' => 'BBB_RECORD_VIDEO_URL_DEBUG',
		        'value' => 'https://media.appswamy.com/presentation/'
		    ],
    	]);
    }
}
