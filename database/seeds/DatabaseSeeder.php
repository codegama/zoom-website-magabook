<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
		$this->call(DemoSeeder::class);
        $this->call(SettingsSeeder::class);
        $this->call(SubscriptionsSeeder::class);
        $this->call(LiveSeeder::class);
        $this->call(BBBSeeder::class);
    }
}
