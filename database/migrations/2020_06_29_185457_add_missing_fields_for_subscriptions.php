<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMissingFieldsForSubscriptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->string('no_of_hrs')->after('no_of_users')->default(1);
            $table->string('no_of_hrs_type')->default(HRS_TYPE_PER_DAY);
        });

        Schema::table('user_subscriptions', function (Blueprint $table) {
            $table->string('no_of_users')->after('payment_mode')->default(4);
            $table->string('no_of_hrs')->after('no_of_users')->default(1);
            $table->string('no_of_hrs_type')->default(HRS_TYPE_PER_DAY);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->dropColumn('no_of_hrs');
            $table->dropColumn('no_of_hrs_type');
        });

        Schema::table('user_subscriptions', function (Blueprint $table) {
            $table->dropColumn('no_of_users');
            $table->dropColumn('no_of_hrs');
            $table->dropColumn('no_of_hrs_type');
        });

    }
}
