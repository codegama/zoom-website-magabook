<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUserDeviceAndRegistrationType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `users` CHANGE `register_type` `register_type` ENUM('web', 'android', 'ios') NOT NULL DEFAULT 'web';");
        DB::statement("ALTER TABLE `users` CHANGE `device_type` `device_type` ENUM('web', 'android', 'ios') NOT NULL DEFAULT 'web';");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    
    }
}
