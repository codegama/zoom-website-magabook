@extends('layouts.admin')

@section('page_header',tr('users'))

@section('styles')

<link rel="stylesheet" href="{{asset('admin-assets/css/dropify.min.css')}}">

<link href="{{asset('admin-assets/css/datepicker.css')}}" rel="stylesheet">

@endsection

@section('breadcrumbs')

<li class="breadcrumb-item"><a href="{{route('admin.users.index')}}">{{tr('users')}}</a></li>

<li class="breadcrumb-item active"><a href="javascript:void(0)"></a> {{tr('edit_user')}}</li>

@endsection

@section('content')

<div class="card">

    <div class="card-header bg-info">

        <h4 class="m-b-0 text-white">{{tr('edit_user')}}</h4>
        
    </div>

    @include('admin.users._form')

</div>

@endsection

@section('scripts')

<script>
    jQuery(document).ready(function() {
        jQuery('input[name="dob"]').daterangepicker({
            autoUpdateInput: false,
            singleDatePicker: true,
            locale: {
                cancelLabel: 'Clear',
                format: 'YYYY-MM-DD'
            }
        });
        jQuery('input[name="dob"]').on('apply.daterangepicker', function(ev, picker) {
            jQuery(this).val(picker.startDate.format('YYYY-MM-DD'));
        });
        jQuery('input[name="dob"]').on('cancel.daterangepicker', function(ev, picker) {
            jQuery(this).val('');
        });
    });
</script>
@endsection