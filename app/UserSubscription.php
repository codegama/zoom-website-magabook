<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSubscription extends Model
{

	protected $appends = ['amount_formatted'];

    public function getAmountFormattedAttribute() {

        return formatted_amount($this->amount  ?? 0.00);
    }

    /**
     * Get the User that owns the UserSubscription.
     */
    public function user() {

        return $this->belongsTo(User::class, 'user_id');

    }

    /**
     * Get the Subscription that owns the UserSubscription.
     */
    public function subscription() {

        return $this->belongsTo(Subscription::class, 'subscription_id');
    }
    
	/**
     * Scope a query to basic subscription details
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeBaseResponse($query) {

        $currency = \Setting::get('currency' , '$');

    	return $query->leftJoin('subscriptions', 'subscriptions.id', '=', 'subscription_id')
            ->select(
                'user_subscriptions.*',
                'user_subscriptions.id as user_subscription_id',
                'subscriptions.title as title',
                'subscriptions.description as description',
                'subscriptions.popular_status as popular_status',
                'subscriptions.plan',
                \DB::raw("'$' as currency")
            );
    }
    

}
