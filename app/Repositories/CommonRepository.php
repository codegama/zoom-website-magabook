<?php

namespace App\Repositories;

use App\Helpers\Helper;

use App\Helpers\HostHelper;

use Log, Validator, Setting;

use App\User;

use Carbon\Carbon;

class CommonRepository {

	/**
     * @method meetings_users_limit()
     *
     * @uses whether the user join, chek the user limit
     *
     * @created vithya
     *
     * @updated vithya
     *
     * @param object user form data
     *
     * @return json data
     *
     */

	public static function meetings_users_limit($meeting_details) {

		$user_subscription_payment = \App\UserSubscription::where('user_id', $meeting_details->user_id)->where('is_current_subscription', YES)->first();

		$no_of_users_allowed = $user_subscription_payment->no_of_users ?? 4;

		$current_users = \App\MeetingMember::where('meeting_id', $meeting_details->id)->where('status', MEETING_MEMBER_JOINED)->count();

		if($current_users >= $no_of_users_allowed) {

		}
	}

	/**
     * @method meetings_eligibility_check()
     *
     * @uses whether the user eligible for create meeting
     *
     * @created vithya
     *
     * @updated vithya
     *
     * @param object user form data
     *
     * @return json data
     *
     */

	public static function meetings_eligibility_check($user_id) {

		$user_subscription = \App\UserSubscription::where('user_id', $user_id)->where('is_current_subscription', YES)->first();

        $total_duration_allowed = $user_subscription->no_of_hrs ?? 1;

        $total_duration_allowed_in_minutes = $total_duration_allowed * 60;

        $meeting_duration_list = \App\Meeting::whereDate('created_at', Carbon::today())->where('user_id', $user_id)->pluck('call_duration')->toArray();

        $used_duration_in_minutes = 0;

        if($meeting_duration_list) {

            $meeting_duration_list = null_safe($meeting_duration_list);

            $used_duration_in_minutes = $meeting_duration_list ? Helper::get_total_time($meeting_duration_list) : 0;
        }

        if($used_duration_in_minutes >= $total_duration_allowed_in_minutes) {

	        $response = ['success' => false, 'error' => api_error(139), 'error_code' => 139];
        
        } else {

	        $data['no_of_users'] = $user_subscription->no_of_users ?? 4;

	        $data['total_duration_allowed_in_minutes'] = $total_duration_allowed_in_minutes;

	        $data['used_duration_in_minutes'] = $used_duration_in_minutes;

	        $data['remaining_hrs_allowed_in_minutes'] = $total_duration_allowed_in_minutes - $used_duration_in_minutes;

	        $response = ['success' => true, 'data' => $data];

	    }

        return response()->json($response, 200);

	}

}