<?php

use App\Helpers\Helper;

use Carbon\Carbon;

use App\User;

use App\MobileRegister, App\PageCounter, App\Settings;

/**
 * @method tr()
 *
 * Description: used to convert the string to language based string
 *
 * @created Vidhya R
 *
 * @updated
 *
 * @param string $key
 *
 * @return string value
 */
function tr($key , $additional_key = "" , $lang_path = "messages.") {

    // if(Auth::guard('admin')->check()) {

    //     $locale = config('app.locale');

    // } else {

        if (!\Session::has('locale')) {

            $locale = \Session::put('locale', config('app.locale'));

        }else {

            $locale = \Session::get('locale');

        }
    // }
    return \Lang::choice('messages.'.$key, 0, Array('other_key' => $additional_key), $locale);
}
/**
 * @method envfile()
 *
 * Description: get the configuration value from .env file 
 *
 * @created Vidhya R
 *
 * @updated
 *
 * @param string $key
 *
 * @return string value
 */

function envfile($key) {

    $data = getEnvValues();

    if($data) {
        return $data[$key];
    }

    return "";

}


/**
 * Function Name : convertMegaBytes()
 * Convert bytes into mega bytes
 *
 * @return number
 */
function convertMegaBytes($bytes) {
    return number_format($bytes / 1048576, 2);
}


/**
 * Check the default subscription is enabled by admin
 *
 */

function user_type_check($user) {

    $user = User::find($user);

    if($user) {

        // if(Setting::get('is_default_paid_user') == 1) {

        //     $user->user_type = 1;

        // } else {

            // User need subscripe the plan

            // if(Setting::get('is_subscription')) {

            //     $user->user_type = 1;

            // } else {
                // Enable the user as paid user
                $user->user_type = 0;
            // }

        // }

        $user->save();

    }

}


function getEnvValues() {

    $data =  [];

    $path = base_path('.env');

    if(file_exists($path)) {

        $values = file_get_contents($path);

        $values = explode("\n", $values);

        foreach ($values as $key => $value) {

            $var = explode('=',$value);

            if(count($var) == 2 ) {
                if($var[0] != "")
                    $data[$var[0]] = $var[1] ? $var[1] : null;
            } else if(count($var) > 2 ) {
                $keyvalue = "";
                foreach ($var as $i => $imp) {
                    if ($i != 0) {
                        $keyvalue = ($keyvalue) ? $keyvalue.'='.$imp : $imp;
                    }
                }
                $data[$var[0]] = $var[1] ? $keyvalue : null;
            }else {
                if($var[0] != "")
                    $data[$var[0]] = null;
            }
        }

        array_filter($data);
    
    }

    return $data;

}

/**
 * @method register_mobile()
 *
 * Description: Update the user register device details 
 *
 * @created Vidhya R
 *
 * @updated
 *
 * @param string $device_type
 *
 * @return - 
 */

function register_mobile($device_type) {

    if($reg = MobileRegister::where('type' , $device_type)->first()) {

        $reg->count = $reg->count + 1;

        $reg->save();
    }
    
}

/**
 * Function Name : subtract_count()
 *
 * Description: While Delete user, subtract the count from mobile register table based on the device type
 *
 * @created vithya R
 *
 * @updated vithya R
 *
 * @param string $device_ype : Device Type (Andriod,web or IOS)
 * 
 * @return boolean
 */

function subtract_count($device_type) {

    if($reg = MobileRegister::where('type' , $device_type)->first()) {

        $reg->count = $reg->count - 1;
        
        $reg->save();
    }

}

/**
 * @method get_register_count()
 *
 * Description: Get no of register counts based on the devices (web, android and iOS)
 *
 * @created Vidhya R
 *
 * @updated
 *
 * @param - 
 *
 * @return array value
 */

function get_register_count() {

    $ios_count = MobileRegister::where('type' , 'ios')->get()->count();

    $android_count = MobileRegister::where('type' , 'android')->get()->count();

    $web_count = MobileRegister::where('type' , 'web')->get()->count();

    $total = $ios_count + $android_count + $web_count;

    return array('total' => $total , 'ios' => $ios_count , 'android' => $android_count , 'web' => $web_count);

}

/**
 * @method: last_x_days_page_view()
 *
 * @uses: to get last x days page visitors analytics
 *
 * @created Anjana H
 *
 * @updated Anjana H
 *
 * @param - 
 *
 * @return array value
 */
function last_x_days_page_view($days){

    $views = PageCounter::orderBy('created_at','asc')->where('created_at', '>', Carbon::now()->subDays($days))->where('page','home');
 
    $arr = array();
 
    $arr['count'] = $views->count();

    $arr['get'] = $views->get();

      return $arr;
}

/**
 * @method: get_hosts_count()
 *
 * @uses: to get host analytics as verified,unverified,total counts
 *
 * @created Anjana H
 *
 * @updated Anjana H
 *
 * @param - 
 *
 * @return array value
 */
function get_hosts_count() {

    $verified_count = Host::where('is_admin_verified' , ADMIN_HOST_APPROVED)->get()->count();

    $unverified_count = Host::where('is_admin_verified' , ADMIN_HOST_PENDING)->get()->count();

    $total = $verified_count + $unverified_count;

    return array('total' => $total , 'verified_count' => $verified_count , 'unverified_count' => $unverified_count);
}

function counter($page){

    $count_home = PageCounter::wherePage($page)->where('created_at', '>=', new DateTime('today'));

        if($count_home->count() > 0) {
            $update_count = $count_home->first();
            $update_count->unique_id = uniqid();
            $update_count->count = $update_count->count + 1;
            $update_count->save();
        } else {
            $create_count = new PageCounter;
            $create_count->page = $page;
            $create_count->unique_id = uniqid();
            $create_count->count = 1;
            $create_count->save();
        }

}

//this function convert string to UTC time zone

function convertTimeToUTCzone($str, $userTimezone, $format = 'Y-m-d H:i:s') {

    $new_str = new DateTime($str, new DateTimeZone($userTimezone));

    $new_str->setTimeZone(new DateTimeZone('UTC'));

    return $new_str->format( $format);
}

//this function converts string from UTC time zone to current user timezone

function convertTimeToUSERzone($str, $userTimezone, $format = 'Y-m-d H:i:s') {

    if(empty($str)){
        return '';
    }
    
    try {
        
        $new_str = new DateTime($str, new DateTimeZone('UTC') );
        
        $new_str->setTimeZone(new DateTimeZone( $userTimezone ));
    }
    catch(\Exception $e) {
        // Do Nothing
    }
    
    return $new_str->format( $format);
}

function number_format_short( $n, $precision = 1 ) {

    if ($n < 900) {
        // 0 - 900
        $n_format = number_format($n, $precision);
        $suffix = '';
    } else if ($n < 900000) {
        // 0.9k-850k
        $n_format = number_format($n / 1000, $precision);
        $suffix = 'K';
    } else if ($n < 900000000) {
        // 0.9m-850m
        $n_format = number_format($n / 1000000, $precision);
        $suffix = 'M';
    } else if ($n < 900000000000) {
        // 0.9b-850b
        $n_format = number_format($n / 1000000000, $precision);
        $suffix = 'B';
    } else {
        // 0.9t+
        $n_format = number_format($n / 1000000000000, $precision);
        $suffix = 'T';
    }
  // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
  // Intentionally does not affect partials, eg "1.50" -> "1.50"
    if ( $precision > 0 ) {
        $dotzero = '.' . str_repeat( '0', $precision );
        $n_format = str_replace( $dotzero, '', $n_format );
    }
    return $n_format . $suffix;

}

function common_date($date , $timezone , $format = "d M Y h:i A") {

    if($timezone) {

        $date = convertTimeToUSERzone($date , $timezone , $format);

    }

    return date($format , strtotime($date));
}

/**
 * function delete_value_prefix()
 * 
 * @uses used for concat string, while deleting the records from the table
 *
 * @created vidhya R
 *
 * @updated vidhya R
 *
 * @param $prefix - from settings table (Setting::get('prefix_user_delete'))
 *
 * @param $primary_id - Primary ID of the delete record
 *
 * @param $is_email 
 *
 * @return concat string based on the input values
 */

function delete_value_prefix($prefix , $primary_id , $is_email = 0) {

    if($is_email) {

        $site_name = str_replace(' ', '_', Setting::get('site_name'));

        return $prefix.$primary_id."@".$site_name.".com";
        
    } else {
        return $prefix.$primary_id;

    }

}

/**
 * function routefreestring()
 * 
 * @uses used for remove the route parameters from the string
 *
 * @created vidhya R
 *
 * @updated vidhya R
 *
 * @param string $string
 *
 * @return Route parameters free string
 */

function routefreestring($string) {

    $string = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $string));
    
    $search = [' ', '&', '%', "?",'=','{','}','$'];

    $replace = ['-', '-', '-' , '-', '-', '-' , '-','-'];

    $string = str_replace($search, $replace, $string);

    return $string;
    
}

/**
 * function amount_format()
 * 
 * @uses used for remove the route parameters from the string
 *
 * @created vidhya R
 *
 * @updated vidhya R
 *
 * @param string $string
 *
 * @return Route parameters free string
 */

function amount_format($amount = 0.00) {

     return number_format((float)$amount, 2, '.', '');
}


/**
 * Function Name : showEntries()
 *
 * To load the entries of the row
 *
 * @created_by Maheswari
 *
 * @updated_by Anjana
 *
 * @return reponse of serial number
 */
function showEntries($request, $i) {

    $s_no = $i;

    // Request Details + s.no

    if (isset($request['page'])) {

        $s_no = (($request['page'] * 10) - 10 ) + $i;

    }

    return $s_no;

}

function array_search_partial($listArray, $keyword) {

    $data = [];

    foreach($listArray as $index => $value) {
 
        if (strpos($index, $keyword) !== FALSE) {

            $key = str_replace('amenties_', "", $index);

            $data[$key] = $value;
        }

    }

    return $data;
}
/**
 * @method selected()
 *
 * @uses set selected item 
 *
 * @created Anjana H
 *
 * @updated Anjana H
 *
 * @param $array, $id, $check_key_name
 *
 * @return response of array 
 */
function selected($array, $id, $check_key_name) {
    
    $is_key_array = is_array($id);
    
    foreach ($array as $key => $array_details) {

        $array_details->is_selected = ($array_details->$check_key_name == $id) ? YES : NO;
    }  

    return $array;
}


function nFormatter($num, $currency = "") {

    $currency = Setting::get('currency', "$");

    if($num>1000) {

        $x = round($num);

        $x_number_format = number_format($x);

        $x_array = explode(',', $x_number_format);

        $x_parts = ['k', 'm', 'b', 't'];

        $x_count_parts = count($x_array) - 1;

        $x_display = $x;

        $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');

        $x_display .= $x_parts[$x_count_parts - 1];

        return $currency." ".$x_display;

    }

    return $currency." ".$num;
}

/**
 * @method formatted_plan()
 *
 * @uses used to format the number
 *
 * @created Bhawya
 *
 * @updated
 *
 * @param integer $num
 * 
 * @param string $currency
 *
 * @return string $formatted_plan
 */

function formatted_plan($plan = 0, $type = "month") {

    $text = $plan <= 1 ? tr('month') : tr('months');

    return $plan." ".$text;
}

/**
 * @method formatted_amount()
 *
 * @uses used to format the number
 *
 * @created Bhawya
 *
 * @updated
 *
 * @param integer $num
 * 
 * @param string $currency
 *
 * @return string $formatted_amount
 */

function formatted_amount($amount = 0.00, $currency = "") {

    $currency = $currency ?: Setting::get('currency', '$');

    $amount = number_format((float)$amount, 2, '.', '');

    $formatted_amount = $currency."".$amount ?: "0.00";

    return $formatted_amount;
}


function api_success($key , $other_key = "" , $lang_path = "messages.") {

    if (!\Session::has('locale')) {

        $locale = \Session::put('locale', config('app.locale'));

    } else {

        $locale = \Session::get('locale');

    }
    return \Lang::choice('api-success.'.$key, 0, Array('other_key' => $other_key), $locale);
}

function api_error($key , $other_key = "" , $lang_path = "messages.") {

    if (!\Session::has('locale')) {

        $locale = \Session::put('locale', config('app.locale'));

    } else {

        $locale = \Session::get('locale');

    }
    return \Lang::choice('api-error.'.$key, 0, Array('other_key' => $other_key), $locale);
}



function readFileLength($file)  {

    $variableLength = 0;
    if (($handle = fopen($file, "r")) !== FALSE) {
         $row = 1;
         while (($data = fgetcsv($handle, 1000, "\n")) !== FALSE) {
            $num = count($data);
            $row++;
            for ($c=0; $c < $num; $c++) {
                $exp = explode("=>", $data[$c]);
                if (count($exp) == 2) {
                    $variableLength += 1; 
                }
            }
        }
        fclose($handle);
    }

    return $variableLength;
}

/**
 * @method meeting_status()
 *
 * @uses Meeting Status
 *
 * @created Bhawya
 *
 * @updated
 * 
 * @param string $meeting_status
 *
 * @return string $meeting_status_text
 */

function meeting_status($status = MEETING_INITIATED) {

    $status_list = [
        MEETING_INITIATED => tr('meeting_initiated'),
        MEETING_STARTED => tr('meeting_started'),
        MEETING_COMPLETED => tr('meeting_completed'),
    ];

    return isset($status_list[$status]) ? $status_list[$status] : tr('meeting_initiated');

}


function calculate_call_duration($start_time, $end_time) {

    $datetime1 = new DateTime($start_time);

    $datetime2 = new DateTime($end_time);

    $interval = $datetime1->diff($datetime2);

    $duration = $interval->format('%h').":".$interval->format('%i').":00";

    $duration = date('H:i:s', strtotime($duration));

    return $duration;

}

function no_of_users_formatted($no_of_users) {

    $no_of_users = $no_of_users <= 1 ? $no_of_users.' '.tr('no_of_user') : $no_of_users.' '.tr('no_of_user').'s';

    return $no_of_users;

}

function no_of_hrs_formatted($no_of_hrs, $no_of_hrs_type) {

    $no_of_hrs = $no_of_hrs <= 1 ? $no_of_hrs.' '.tr('no_of_hr') : $no_of_hrs.' '.tr('no_of_hr').'s';

    return $no_of_hrs."/".$no_of_hrs_type;

}

function get_bbb_base_url() {

    return Setting::get('BBB_ENV') == 'production' ? Setting::get('BBB_SERVER_BASE_URL_PRODUCTION') : Setting::get('BBB_SERVER_BASE_URL_DEBUG');

}

function get_bbb_secret() {

    return Setting::get('BBB_ENV') == 'production' ? Setting::get('BBB_SECRET_PRODUCTION') : Setting::get('BBB_SECRET_DEBUG');

}

function get_meeting_status_error($status) {

    $status_list = [
        MEETING_NONE => api_error(141), 
        MEETING_SCHEDULED => api_error(141), 
        MEETING_ENDED => api_error(143),
        MEETING_CANCELLED => api_error(144),
    ];

    return isset($status_list[$status]) ? $status_list[$status] : api_error(141);

}

function get_overall_record_url($bbb_record_id) {

    // sample url - https://media.appswamy.com/download/presentation/208f1412c4be94443324808ea1f8efed08170423-1604588349145/208f1412c4be94443324808ea1f8efed08170423-1604588349145.mp4

    $base_url = Setting::get('BBB_ENV') == 'production' ? Setting::get('BBB_RECORD_PRESENTATION_URL_PRO') : Setting::get('BBB_RECORD_PRESENTATION_URL_DEBUG');

    $url = $base_url.$bbb_record_id.'/'.$bbb_record_id.'.mp4';

    return $url ? $url : "";

    // return remote_file_exists($url) ? $url : "";

}

function get_video_record_url($bbb_record_id) {

    // sample url - https://media.appswamy.com/presentation/0ba43374d2ff36f157e8e119f169fc41fd70c917-1604588636015/video/webcams.webm

    $base_url = Setting::get('BBB_ENV') == 'production' ? Setting::get('BBB_RECORD_VIDEO_URL_PRO') : Setting::get('BBB_RECORD_VIDEO_URL_DEBUG');

    $url = $base_url.$bbb_record_id."/video/webcams.webm";

    return $url ? $url : "";

    // return remote_file_exists($url) ? $url : "";

}

function remote_file_exists($url) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    if( $httpCode == 200 ){return true;}
    return false;
}


function null_safe($list) {
    
    foreach($list as $key => $value) {

        if($value === '' || is_null($value)) { 
            unset($list[$key]); 
        } 
    } 
    
    return $list; 
}